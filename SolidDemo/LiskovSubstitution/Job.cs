﻿namespace SolidDemo.LiskovSubstitution
{
    public class Job
    {
        public string Description { get; set; }

        public Status Status { get; set; }
    }

    public enum Status
    {
        Requested,
        Done
    }
}