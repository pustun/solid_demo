﻿using System.Data.Odbc;

namespace SolidDemo.LiskovSubstitution
{
    public class AlcoholicCraftsman : Craftsman
    {
        private readonly ICalendar _calendar;

        public AlcoholicCraftsman(ICalendar calendar)
        {
            _calendar = calendar;
        }

        public override void DoJob(Job job)
        {
            if (_calendar.Today() == "Friday")
            {
                throw new SorryIWantABeerException();
            }

            base.DoJob(job);
        }
    }
}