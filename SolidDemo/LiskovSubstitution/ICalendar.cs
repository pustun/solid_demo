﻿namespace SolidDemo.LiskovSubstitution
{
    public interface ICalendar
    {
        string Today();
    }
}