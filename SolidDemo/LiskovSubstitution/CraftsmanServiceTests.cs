﻿using FluentAssertions;
using Moq;
using Xunit;

namespace SolidDemo.LiskovSubstitution
{
    public class CraftsmanServiceTests
    {
        [Fact]
        public void All_jobs_have_to_be_done()
        {
            // arrange
            var jobs = new[]
            {
                new Job {Description = "Change a lamp", Status = Status.Requested},
                new Job {Description = "Repair fence", Status = Status.Requested}
            };

            var calendarMock = new Mock<ICalendar>();
            calendarMock.Setup(x => x.Today()).Returns("Monday");

            var craftsman = new AlcoholicCraftsman(calendarMock.Object);

            var craftsmanService = new CraftsmanService(craftsman);
            
            // act
            craftsmanService.ProcessRequests(jobs);

            // assert
            jobs.Should().OnlyContain(job => job.Status == Status.Done);
        }
    }
}