﻿namespace SolidDemo.LiskovSubstitution
{
    public class Craftsman
    {
        public virtual void DoJob(Job job)
        {
            job.Status = Status.Done;
        }
    }
}