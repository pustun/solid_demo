﻿using System.Collections.Generic;

namespace SolidDemo.LiskovSubstitution
{
    public class CraftsmanService
    {
        private readonly Craftsman _craftsman;

        public CraftsmanService(Craftsman craftsman)
        {
            _craftsman = craftsman;
        }

        public void ProcessRequests(IEnumerable<Job> pendingJobs)
        {
            foreach (var job in pendingJobs)
            {
                _craftsman.DoJob(job);
            }
        }
    }
}