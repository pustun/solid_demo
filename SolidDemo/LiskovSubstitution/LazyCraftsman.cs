﻿using System;
using System.Threading;

namespace SolidDemo.LiskovSubstitution
{
    public class LazyCraftsman : Craftsman
    {
        public override void DoJob(Job job)
        {
            var sigaretsToSmokeToday = HardDecisionToMake();

            HaveABreak(sigaretsToSmokeToday);

            base.DoJob(job);
        }

        private void HaveABreak(int sigaretsToSmokeToday)
        {
            Thread.Sleep(TimeSpan.FromSeconds(sigaretsToSmokeToday));
        }

        private int HardDecisionToMake()
        {
            return new Random((int) DateTime.Now.Ticks).Next(1, 3);
        }
    }
}