﻿using System;

namespace SolidDemo.SingleResponsibility
{
    public class UserValidator : IUserValidator
    {
        public bool Validate(User user)
        {
            return !string.IsNullOrEmpty(user.Name)
                   && !string.IsNullOrEmpty(user.Email);
        }
    }
}