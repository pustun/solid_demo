﻿using FluentAssertions;
using Xunit;

namespace SolidDemo.SingleResponsibility
{
    public class UserValidationTests
    {
        [Fact]
        public void When_user_has_all_fields_should_be_true()
        {
            // arrange
            var user = new User
            {
                Name = "Max Pecu",
                Email = "max@pecu.de"
            };

            var userValidator = new UserValidator();

            // act
            var isValid = userValidator.Validate(user);

            // assert
            isValid.Should().BeTrue();
        }

        [Fact]
        public void When_name_is_empty_should_be_false()
        {
            // arrange
            var user = new User
            {
                Name = string.Empty,
                Email = "max@pecu.de"
            };

            var userValidator = new UserValidator();
            
            // act
            var isValid = userValidator.Validate(user);

            // assert
            isValid.Should().BeFalse();
        }

        [Fact]
        public void When_email_is_empty_should_be_false()
        {
            // arrange
            var user = new User
            {
                Name = "Max Pecu",
                Email = string.Empty
            };

            var userValidator = new UserValidator();

            // act
            var isValid = userValidator.Validate(user);

            // assert
            isValid.Should().BeFalse();
        }
    }
}