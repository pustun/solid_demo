﻿using System;
using FluentAssertions;
using Moq;
using Xunit;

namespace SolidDemo.SingleResponsibility
{
    public class UserManagementServiceTests
    {
        [Fact]
        public void When_user_is_valid_should_call_user_repository()
        {
            // arrange
            var userName = "Max Pecu";
            var email = "max@pecu.de";

            var userRepositoryMock = new Mock<IUserRepository>();
            userRepositoryMock.Setup(x => x.Save(
                It.Is<User>(u => u.Name == "Max Pecu" && u.Email == "max@pecu.de"))
            );

            var userValidatorMock = new Mock<IUserValidator>();
            userValidatorMock.Setup(x => x.Validate(It.IsAny<User>()))
                .Returns(true);

            var userManagementService = 
                new UserManagementService(userRepositoryMock.Object, userValidatorMock.Object);

            // act
            userManagementService.RegisterUser(userName, email);

            // assert
            userRepositoryMock.VerifyAll();
        }

        [Fact]
        public void Should_throw_validation_exception_if_user_not_valid()
        {
            // arrange
            var userName = (string)null;
            var email = (string)null;

            var userRepositoryMock = new Mock<IUserRepository>();
            userRepositoryMock.Setup(x => x.Save(It.IsAny<User>()));

            var userValidatorMock = new Mock<IUserValidator>();
            userValidatorMock.Setup(x => x.Validate(It.IsAny<User>()))
                .Returns(false);

            var userManagementService = 
                new UserManagementService(userRepositoryMock.Object, userValidatorMock.Object);
            
            // act && assert
            Action actionToCheck = () => userManagementService.RegisterUser(userName, email);

            actionToCheck.ShouldThrow<ValidationException>();

            userRepositoryMock.Verify(x => x.Save(It.IsAny<User>()), Times.Never);
        }
    }
}