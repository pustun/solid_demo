﻿namespace SolidDemo.SingleResponsibility
{
    public class UserManagementService : IUserManagementService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserValidator _userValidator;

        public UserManagementService(IUserRepository userRepository, IUserValidator userValidator)
        {
            _userRepository = userRepository;
            _userValidator = userValidator;
        }

        public void RegisterUser(string name, string email)
        {
            var user = new User {Name = name, Email = email};

            if (!_userValidator.Validate(user))
            {
                throw new ValidationException();
            }

            _userRepository.Save(user);
        }
    }
}
