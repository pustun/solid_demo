﻿namespace SolidDemo.SingleResponsibility
{
    interface IUserManagementService
    {
        void RegisterUser(string name, string email);
    }
}