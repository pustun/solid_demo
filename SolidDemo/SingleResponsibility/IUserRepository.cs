namespace SolidDemo.SingleResponsibility
{
    public interface IUserRepository
    {
        void Save(User user);
    }
}