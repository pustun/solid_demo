﻿using System.Collections.Generic;

namespace SolidDemo.InterfaceSegregation
{
    public interface IDiscountPlan
    {
        void Apply(IEnumerable<Client> clients);
    }
}