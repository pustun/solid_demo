﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SolidDemo.InterfaceSegregation
{
    public class OldCustomerDiscountPlan : IDiscountPlan
    {
        private readonly IDiscountRepository _discountRepository;

        public OldCustomerDiscountPlan(IDiscountRepository discountRepository)
        {
            _discountRepository = discountRepository;
        }

        public void Apply(IEnumerable<Client> clients)
        {
            var enumerable = clients.ToArray();

            var badUsers = enumerable.Where(x => x.RegistrationDate.AddYears(2) > DateTime.Now);

            foreach (var client in enumerable.Except(badUsers))
            {
                _discountRepository.AddDiscount(client.Id, "OldCustomer");
            }
        }
    }
}