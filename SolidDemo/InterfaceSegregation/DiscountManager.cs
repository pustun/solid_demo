﻿using System.Collections.Generic;

namespace SolidDemo.InterfaceSegregation
{
    public class DiscountManager
    {
        private readonly IEnumerable<IDiscountPlan> _discountPlans;

        public DiscountManager(IEnumerable<IDiscountPlan> discountPlans)
        {
            _discountPlans = discountPlans;
        }

        public void CheckDiscountsFor(IList<Client> clients)
        {
            foreach (var discountPlan in _discountPlans)
            {
                discountPlan.Apply(clients);
            }
        }
    }
}