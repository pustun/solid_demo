﻿using System;

namespace SolidDemo.InterfaceSegregation
{
    public static class DateTimeExtension
    {
        public static DateTime YearsAgo(this int years)
        {
            return DateTime.Now.AddYears(-years);
        }
    }
}