﻿namespace SolidDemo.InterfaceSegregation
{
    public interface IDiscountRepository
    {
        void AddDiscount(int clientId, string discountType);
    }
}