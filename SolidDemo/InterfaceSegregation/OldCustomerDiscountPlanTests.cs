﻿using System.Collections.Generic;
using Moq;
using Xunit;

namespace SolidDemo.InterfaceSegregation
{
    public class OldCustomerDiscountPlanTests
    {
        [Fact]
        public void When_customer_was_registered_at_least_2_years_ago_should_get_old_customer_discount()
        {
            // arrange
            var discountRepositoryMock = new Mock<IDiscountRepository>();
            discountRepositoryMock.Setup(x => x.AddDiscount(It.IsAny<int>(), It.IsAny<string>()));

            var activeCustomerDiscountPlan = new OldCustomerDiscountPlan(discountRepositoryMock.Object);

            var clients = new List<Client>
            {
                new Client {Id = 1, NumberOfOrders = 2, RegistrationDate = 2.YearsAgo()},
                new Client {Id = 2, NumberOfOrders = 4, RegistrationDate = 1.YearsAgo()},
                new Client {Id = 3, NumberOfOrders = 1, RegistrationDate = 3.YearsAgo()}
            };

            // act
            activeCustomerDiscountPlan.Apply(clients);

            // assert
            discountRepositoryMock.Verify(x => x.AddDiscount(It.IsAny<int>(), "OldCustomer"), Times.Exactly(2));
        }
    }
}