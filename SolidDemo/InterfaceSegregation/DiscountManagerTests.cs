﻿using System.Collections.Generic;
using Moq;
using Xunit;

namespace SolidDemo.InterfaceSegregation
{
    public class DiscountManagerTests
    {
        [Fact]
        public void Should_call_each_plan_over_all_clients()
        {
            // arrange
            var discountRepositoryMock = new Mock<IDiscountRepository>();
            discountRepositoryMock.Setup(x => x.AddDiscount(It.IsAny<int>(), It.IsAny<string>()));

            var clients = new List<Client>
            {
                new Client {Id = 1, NumberOfOrders = 2, RegistrationDate = 2.YearsAgo()},
                new Client {Id = 2, NumberOfOrders = 4, RegistrationDate = 1.YearsAgo()},
                new Client {Id = 3, NumberOfOrders = 1, RegistrationDate = 3.YearsAgo()}
            };

            var activeCustomerDiscountPlan = new ActiveCustomerDiscountPlan(discountRepositoryMock.Object);
            var oldCustomerDiscountPlan = new OldCustomerDiscountPlan(discountRepositoryMock.Object);

            var discountManager = new DiscountManager(new IDiscountPlan[]
            {
                activeCustomerDiscountPlan,
                oldCustomerDiscountPlan
            });
            
            // act
            discountManager.CheckDiscountsFor(clients);

            // assert
            discountRepositoryMock.Verify(x => x.AddDiscount(It.IsAny<int>(), "ActiveCustomer"), Times.Once);
            discountRepositoryMock.Verify(x => x.AddDiscount(It.IsAny<int>(), "OldCustomer"), Times.Exactly(2));
        }
    }
}