﻿using System;
using System.Collections.Generic;
using Moq;
using Xunit;

namespace SolidDemo.InterfaceSegregation
{
    public class ActiveCustomerDiscountPlanTests
    {
        [Fact]
        public void When_customer_has_at_least_3_orders_should_get_active_customer_discount()
        {
            // arrange
            var discountRepositoryMock = new Mock<IDiscountRepository>();
            discountRepositoryMock.Setup(x => x.AddDiscount(It.IsAny<int>(), It.IsAny<string>()));

            var activeCustomerDiscountPlan = new ActiveCustomerDiscountPlan(discountRepositoryMock.Object);

            var clients = new List<Client>
            {
                new Client {Id = 1, NumberOfOrders = 2, RegistrationDate = 2.YearsAgo()},
                new Client {Id = 2, NumberOfOrders = 4, RegistrationDate = 1.YearsAgo()},
                new Client {Id = 3, NumberOfOrders = 1, RegistrationDate = 3.YearsAgo()}
            };

            // act
            activeCustomerDiscountPlan.Apply(clients);

            // assert
            discountRepositoryMock.Verify(x => x.AddDiscount(It.IsAny<int>(), "ActiveCustomer"), Times.Once);
        }
    }
}