﻿using System;

namespace SolidDemo.InterfaceSegregation
{
    public class Client
    {
        public int Id { get; set; }

        public DateTime RegistrationDate { get; set; }

        public int NumberOfOrders { get; set; }
    }
}