﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SolidDemo.InterfaceSegregation
{
    public class ActiveCustomerDiscountPlan : IDiscountPlan
    {
        private readonly IDiscountRepository _discountRepository;

        public ActiveCustomerDiscountPlan(IDiscountRepository discountRepository)
        {
            _discountRepository = discountRepository;
        }

        public void Apply(IEnumerable<Client> clients)
        {
            var enumerable = clients.ToArray();

            var badUsers = enumerable.Where(x => x.NumberOfOrders < 3);

            foreach (var client in enumerable.Except(badUsers))
            {
                _discountRepository.AddDiscount(client.Id, "ActiveCustomer");
            }
        }
    }
}