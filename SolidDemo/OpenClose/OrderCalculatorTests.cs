﻿using FluentAssertions;
using Xunit;

namespace SolidDemo.OpenClose
{
    public class OrderCalculatorTests
    {
        [Fact]
        public void Should_sum_all_items()
        {
            // arrange
            var order = new Order
            {
                OrderItems = new[]
                {
                    new OrderItem {ItemId = 42, Price = 10.0M, Quantity = 1},
                    new OrderItem {ItemId = 73, Price = 20.0M, Quantity = 2}
                }
            };

            var expectedSum = 10.0M*1 + 20.0M*2;

            var orderCalculator = new OrderCalculator();

            // act
            var actualSum = orderCalculator.Calculate(order);

            // assert
            actualSum.Should().Be(expectedSum);
        }
    }
}