﻿using System.Linq;

namespace SolidDemo.OpenClose
{
    public class OrderCalculator : IOrderCalculator
    {
        public decimal Calculate(Order order)
        {
            return order.OrderItems.Sum(x => x.Price*x.Quantity);
        }
    }
}