﻿namespace SolidDemo.OpenClose
{
    public static class PecentExtension
    {
        public static decimal PercentFrom(this int percent, decimal baseValue)
        {
            return baseValue*percent/100.0m;
        }
    }
}