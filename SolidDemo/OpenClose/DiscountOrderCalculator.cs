﻿namespace SolidDemo.OpenClose
{
    public class DiscountOrderCalculator
    {
        private readonly IOrderCalculator _orderCalculator;

        public DiscountOrderCalculator(IOrderCalculator orderCalculator)
        {
            _orderCalculator = orderCalculator;
        }

        public decimal CalculateWithDiscount(Order order)
        {
            var sum = _orderCalculator.Calculate(order);

            if (sum > 100.0m)
            {
                return sum - 10.PercentFrom(sum);
            }

            return sum;
        }
    }
}