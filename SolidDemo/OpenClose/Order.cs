﻿using System.Collections.Generic;

namespace SolidDemo.OpenClose
{
    public class Order
    {
        public IEnumerable<OrderItem> OrderItems { get; set; }
    }
}