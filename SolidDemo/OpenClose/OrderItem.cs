﻿namespace SolidDemo.OpenClose
{
    public class OrderItem
    {
        public int ItemId { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }
    }
}