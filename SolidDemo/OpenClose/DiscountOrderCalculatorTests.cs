﻿using FluentAssertions;
using Moq;
using Xunit;

namespace SolidDemo.OpenClose
{
    public class DiscountOrderCalculatorTests
    {
        [Fact]
        public void When_sum_less_than_100_should_not_apply_discount()
        {
            // arrange
            var orderCalculatorMock = new Mock<IOrderCalculator>();
            orderCalculatorMock.Setup(x => x.Calculate(It.IsAny<Order>()))
                .Returns(80.0m);

            var discountOrderCalculator = new DiscountOrderCalculator(orderCalculatorMock.Object);

            // act
            var sumWithDiscount = discountOrderCalculator.CalculateWithDiscount(null);

            // assert
            sumWithDiscount.Should().Be(80.0M);
        }

        [Fact]
        public void When_sum_greater_than_100_should_apply_10_percent_discount()
        {
            // arrange
            var orderCalculatorMock = new Mock<IOrderCalculator>();
            orderCalculatorMock.Setup(x => x.Calculate(It.IsAny<Order>()))
                .Returns(130.0m);

            var discountOrderCalculator = new DiscountOrderCalculator(orderCalculatorMock.Object);

            // act
            var sumWithDiscount = discountOrderCalculator.CalculateWithDiscount(null);

            // assert
            sumWithDiscount.Should().Be(130.0M - 13.0M);
        }
    }
}