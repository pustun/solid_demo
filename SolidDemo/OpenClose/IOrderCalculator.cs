﻿namespace SolidDemo.OpenClose
{
    public interface IOrderCalculator
    {
        decimal Calculate(Order order);
    }
}